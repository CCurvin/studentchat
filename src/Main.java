import java.util.ArrayList;
import javax.swing.JFrame;

public class Main {
	
	public static void main (String[] args) {
		
		JFrame frame = new JFrame("Boom! Window");
		frame.setSize(500, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		ArrayList<Student> Students = new ArrayList<Student>();
		
		Student s0 = new Student("Hayden", "Beadles");
		Students.add(s0);
		
		Student s1 = new Student("Gerald", "Carson");
		Students.add(s1);	
		
		for (int i=0; i<s0.chatArr.length; i++) {
			System.out.print(s0.getName() + ": " + s0.letsChat());
			System.out.println();			
			System.out.print(s1.getName() + ": " + s1.letsChat());
			System.out.println();
		}
		System.out.println();
		
		Student s2 = new Student("Chad", "Curvin");
		Students.add(s2);
		
		Student s3 = new Student("Jayci", "Giles");
		Students.add(s3);
		
		for (int i=0; i<s2.chatArr.length; i++) {
			System.out.print(s2.getName() + ": " + s2.letsChat());
			System.out.println();			
			System.out.print(s3.getName() + ": " + s3.letsChat());
			System.out.println();
		}
		System.out.println();
		
		Student s4 = new Student("Ian", "Hale");
		Students.add(s4);
		
		Student s5 = new Student("Joseph", "Hubbard");
		Students.add(s5);
		
		for (int i=0; i<s4.chatArr.length; i++) {
			System.out.print(s4.getName() + ": " + s4.letsChat());
			System.out.println();			
			System.out.print(s5.getName() + ": " + s5.letsChat());
			System.out.println();
		}
		System.out.println();
		
		Student s6 = new Student("Jose", "Jara");
		Students.add(s6);
		
		Student s7 = new Student("Jonathan", "Langford");
		Students.add(s7);
		
		for (int i=0; i<s6.chatArr.length; i++) {
			System.out.print(s6.getName() + ": " + s6.letsChat());
			System.out.println();			
			System.out.print(s7.getName() + ": " + s7.letsChat());
			System.out.println();
		}
		System.out.println();
		
		Student s8 = new Student("Trevor", "Marsh");
		Students.add(s8);
		
		Student s9 = new Student("Casey", "Mau");
		Students.add(s9);
		
		for (int i=0; i<s8.chatArr.length; i++) {
			System.out.print(s8.getName() + ": " + s8.letsChat());
			System.out.println();			
			System.out.print(s9.getName() + ": " + s9.letsChat());
			System.out.println();
		}
		System.out.println();
		
		Student s10 = new Student("Bryant", "Morrill");
		Students.add(s10);
		
		Student s11 = new Student("Mark", "Richardson");
		Students.add(s11);
		
		for (int i=0; i<s10.chatArr.length; i++) {
			System.out.print(s10.getName() + ": " + s10.letsChat());
			System.out.println();			
			System.out.print(s11.getName() + ": " + s11.letsChat());
			System.out.println();
		}
		System.out.println();
		
		Student s12 = new Student("Nash", "Stewart");
		Students.add(s12);
		
		Student s13 = new Student("Michael", "Zeller");
		Students.add(s13);
		
		for (int i=0; i<s12.chatArr.length; i++) {
			System.out.print(s12.getName() + ": " + s12.letsChat());
			System.out.println();			
			System.out.print(s13.getName() + ": " + s13.letsChat());
			System.out.println();
		}
	}
}