import java.util.Random;

public class Student {
	private String firstName;
	private String lastName;
	
	private double score = 100.00;
	
	public String chatArr[] = {
			"I like pie.",
			"Hello, my name is",
			"Java's got nothing on me",
			"OOP is not always the solution",
			"Blastoff! (straight from the book, here)"};	

	public Student(String firstName, String lastName, double score) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.score = score;
	}
	
	public Student(String fname, String lname) {
		setFirstName(fname);
		setLastName(lname);
	}
	
	public Student() {
		this.firstName = "Sample";
		this.lastName = "Sam";
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		String name = this.firstName + " " + this.lastName;
		return name;
	}
	
	public void printName() {
		System.out.println(getName());
	}
	
	public void setScore (double sc) {
		this.score = sc;
	}
	
	public double getScore() {
		return this.score;
	}
	
	public String letsChat() {
		
		Random rand = new Random();
		int num = rand.nextInt(5);
		//System.out.println(chatArr[num]);
		return chatArr[num];
	}
	
}